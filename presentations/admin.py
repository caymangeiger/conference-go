from django.contrib import admin

from .models import Presentation, Status


@admin.register(Presentation)
class PresentationAdmin(admin.ModelAdmin):
    list_display = [
        "title",
        "status",
        "created"
]


@admin.register(Status)
class StatusAdmin(admin.ModelAdmin):
    pass
