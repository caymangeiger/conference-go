from django.http import JsonResponse
from .models import Conference, Location, State
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from .acl import get_photo, get_weather_data
# class StateEncoder(ModelEncoder):
#     model = State
#     properties = [
#         "abbreviation",
#         "name"
# ]


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "room_count",
        "created",
        "updated",
        "city",
        "state",
        "picture_url",
    ]
    # encoders = {
    #         "state": StateEncoder(),
    #     }


    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "state",
        "picture_url",

    ]

    # encoders = {
    #         "state": StateEncoder(),
    #     }

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
        "temp",
        "weather_description",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "temp",
        "weather_description",
    ]

@require_http_methods(["POST", "GET"])
def api_list_conferences(request):
    """

    Lists the conference names and the link to the conference.

    Returns a dictionary with a single key "conferences" which
    is a list of conference names and URLS. Each entry in the list
    is a dictionary that contains the name of the conference and
    the link to the conference's information.

    {
        "conferences": [
            {
                "name": conference's name,
                "href": URL to the conference,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        weather_description, temp_kelvin = get_weather_data(content["location"]["city"], content["location"]["state"])
        temp_fahrenheit = (temp_kelvin - 273.15) * 9/5 + 32
        content["temp"] = temp_fahrenheit
        content["weather_description"] = weather_description
        try:
            location = Location.objects.get(name=content["location"]["name"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse({"message": "Invalid location id"}, status=400)
        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_conference(request, id):
    """

    Returns the details for the Conference model specified
    by the id parameter.

    This should return a dictionary with the name, starts,
    ends, description, created, updated, max_presentations,
    max_attendees, and a dictionary for the location containing
    its name and href.

    {
        "name": the conference's name,
        "starts": the date/time when the conference starts,
        "ends": the date/time when the conference ends,
        "description": the description of the conference,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "max_presentations": the maximum number of presentations,
        "max_attendees": the maximum number of attendees,
        "location": {
            "name": the name of the location,
            "href": the URL for the location,
        }
    }
    """
    if request.method == "GET":
        conference = Conference.objects.get(id=id)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            conference = Location.objects.get(name=content["location"]["name"])
            content["location"] = conference
        except Location.DoesNotExist:
            return JsonResponse({"message": "Invalid location id"}, status=400)
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["location"]["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        Conference.objects.filter(id=id).update(**content)
        conference = Location.objects.get(id=id)
        return JsonResponse(
            conference,
            encoder=LocationDetailEncoder,
            safe=False,
        )


@require_http_methods(["POST", "GET"])
def api_list_locations(request):
    """
    Lists the location names and the link to the location.

    Returns a dictionary with a single key "locations" which
    is a list of location names and URLS. Each entry in the list
    is a dictionary that contains the name of the location and
    the link to the location's information.

    {
        "locations": [
            {
                "name": location's name,
                "href": URL to the location,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        content["picture_url"] = get_photo(content["city"], content["state"])
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content['state'])
                content['state'] = state
        except State.DoesNotExist:
            return JsonResponse({"Message": "This does not exist"}, status=418)
        location = Location.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationListEncoder,
            safe=False
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    """

    Returns the details for the Location model specified
    by the id parameter.

    This should return a dictionary with the name, city,
    room count, created, updated, and state abbreviation.

    {
        "name": location's name,
        "city": location's city,
        "room_count": the number of rooms available,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "state": the two-letter abbreviation for the state,
    }
    """
    if request.method == "GET":
        location = Location.objects.filter(id=id)
        return JsonResponse(
            {"location": location},
            encoder=LocationDetailEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        Location.objects.filter(id=id).update(**content)
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
