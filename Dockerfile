FROM python:3
# Finds and downloads a dockerhub image

# ENV PYTHONUNBUFFERED 1
# Create an environmental variable

WORKDIR /app
# Creates a working directory on the container

# COPY . .
# Copies files from our local machine to the container

# Copy all the code from the local directory into the image
COPY accounts accounts
COPY attendees attendees
COPY common common
COPY conference_go conference_go
COPY events events
COPY presentations presentations
COPY requirements.txt requirements.txt
COPY manage.py manage.py


RUN pip install -r requirements.txt
# Runs a command on the terminal

# CMD python manage.py runserver 0.0.0.0:8000
# Runs our server
CMD gunicorn --bind 0.0.0.0:8000 conference_go.wsgi
